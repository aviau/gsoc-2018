# GSOC 2018 Project of Alexandre Viau

This repository serves as my final GSOC submission. It references all the work
that has been done and points to the right places.

Project title: Autodeb - Automatic Packages For Everything

GSOC Website Project URL: https://summerofcode.withgoogle.com/projects/#5560246244737024

Email: alexandre@alexandreviau.net

Student Debian Wiki Page: https://wiki.debian.org/aviau

Student blog: https://alexandreviau.net/blog/

Project presentation at DebConf18 in Taiwan: https://meetings-archive.debian.net/pub/debian-meetings/2018/DebConf18/2018-07-31/autodeb-automatic-packages-for-everythin.webm

Project repositories:
 - main repository: https://salsa.debian.org/autodeb-team/autodeb
 - infrastructure scripts: https://salsa.debian.org/autodeb-team/infrastructure
 - packaging: https://salsa.debian.org/autodeb-team/autodeb-packaging

As of 2018-08-09, all of the code contained in the above repositories was written by me
as part of Google Summer of Code.

This repository also contains a "contributions" folder. It contains patches that I have
submitted to other projects during GSOC. The patches have an "Applied-Upstream" header
pointing to their merged commits.

# Conclusion

## What was done

Autodeb is composed of two components, the master(s) and the worker(s).

The service has an upload queue that accepts only source uploads. It will build and test the uploaded packages and optionally forward them to ftpmaster.

The service can produce large numbers of atomatic backports and package upgrades. The resulting packages are hosted in apt repositories at https://auto.debian.net/repos/.

On auto.debian.net, users log in with their salsa.debian.org account. However, autodeb supports any oauth2 server as an authentification backend.

The project was presented during DebConf18 in Taiwan.

## Ideas for the future / what is left to do

- Automatically detect new backport and package upgrade candidates and create new jobs
- Create pages for every packages, with links to: binaries, build logs, and controls to launch new jobs.
- Create and host API documentation
